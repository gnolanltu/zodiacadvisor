package edu.ltu.horoscope;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity {
    private DailyHoroscope zodiacApi = new DailyHoroscope();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickGetHoroscope(View view) {
        TextView horoscope = (TextView) findViewById(R.id.daily_horoscope);

        Spinner signs = (Spinner) findViewById(R.id.signs);

        String starSign = String.valueOf(signs.getSelectedItem());

        String dailyZodiac = zodiacApi.getDailyHoroscope(starSign);

        horoscope.setText(dailyZodiac);
    }
}